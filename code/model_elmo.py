import tensorflow as tf
import tensorflow.keras as K
import numpy as np
import random
from utils import *
from sklearn.metrics import confusion_matrix
from nltk.corpus import wordnet as wn
import tensorflow_hub as hub
import random


MAX_LENGTH = 60
EMBEDDING_SIZE = 200
HIDDEN_SIZE = 256



def create_tensorflow_model(train_vocab_size, labelF_vocab_size, labelC_vocab_size, embedding_size, hidden_size, use_attention, use_elmo):
    """
    The model of the RNN
    """
    print("===== Creating TENSORFLOW model =====")
     
    # hidden size for each layer, number of elements in the vector define the number of layers (only for multilayer LSTM)
    layers_hidden_sizes = [HIDDEN_SIZE, HIDDEN_SIZE]
    # fine grained Labels have (batch_size,) shape.
    labelsF = tf.placeholder(tf.int64, shape=[None, MAX_LENGTH])
    # corse grained Labels have (batch_size,) shape.
    labelsC = tf.placeholder(tf.int64, shape=[None, MAX_LENGTH])
    # fine grained sense_mask
    sense_maskF = tf.placeholder(tf.float32, shape=[None, MAX_LENGTH, labelF_vocab_size])
    # corse grained sense_mask
    sense_maskC = tf.placeholder(tf.float32, shape=[None, MAX_LENGTH, labelC_vocab_size])
    # Keep_prob is a scalar.
    keep_prob = tf.placeholder(tf.float32, shape=[])
    #sequence length
    seq_length = tf.count_nonzero(labelsF, axis=-1, dtype=tf.int32)
    #mask for padding
    mask_weights=tf.to_float(tf.not_equal(labelsF, 0))
    #parameters for attention layer
    attention_size = HIDDEN_SIZE*2
    epsilon = 1e-8



   #inputs and embeddings layer are different if it is using elmo or not
    if use_elmo==True:
        with tf.variable_scope("embeddings"):
            # inputs have (batch_size, timesteps) shape.
            inputs = tf.placeholder(tf.string, shape=[None, MAX_LENGTH])
            elmo = hub.Module("https://tfhub.dev/google/elmo/1", trainable=False)
            tokens_input=inputs
            tokens_length=seq_length
            embeddings = elmo(inputs={"tokens": tokens_input, "sequence_len": tokens_length}, signature="tokens", as_dict=True)["elmo"]
    else:
        with tf.variable_scope("embeddings"):
            # inputs have (batch_size, timesteps) shape.
            inputs = tf.placeholder(tf.int64, shape=[None, MAX_LENGTH])
            embedding_matrix = tf.get_variable("embeddings", shape=[train_vocab_size, embedding_size])
            embeddings = tf.nn.embedding_lookup(embedding_matrix, inputs)


    with tf.variable_scope("rnn"):

        # single layer
        # rnn_cell = tf.nn.rnn_cell.LSTMCell(HIDDEN_SIZE, initializer=tf.contrib.layers.xavier_initializer())
        # rnn_cell = tf.nn.rnn_cell.DropoutWrapper(rnn_cell,
        #                                          input_keep_prob=keep_prob,
        #                                          output_keep_prob=keep_prob,
        #                                          state_keep_prob=keep_prob)

        # (fw_outputs, bw_outputs), states = tf.nn.bidirectional_dynamic_rnn(rnn_cell, rnn_cell,
        #                              embeddings, sequence_length=seq_length, dtype=tf.float32)

        # outputs = tf.concat([fw_outputs, bw_outputs], axis=2)


        # two staked layers
        cells_fw = []
        cells_bw = []
        for i in range(len(layers_hidden_sizes)):
            cell_fw = tf.nn.rnn_cell.LSTMCell(layers_hidden_sizes[i],
                                    initializer=tf.contrib.layers.xavier_initializer())
            cell_fw = tf.nn.rnn_cell.DropoutWrapper(cell_fw,
                                                    input_keep_prob=keep_prob,
                                                    output_keep_prob=keep_prob,
                                                    state_keep_prob=keep_prob)
            cells_fw.append(cell_fw)
            cell_bw = tf.nn.rnn_cell.LSTMCell(layers_hidden_sizes[i],
                                    initializer=tf.contrib.layers.xavier_initializer())
            cell_bw = tf.nn.rnn_cell.DropoutWrapper(cell_bw,
                                                    input_keep_prob=keep_prob,
                                                    output_keep_prob=keep_prob,
                                                    state_keep_prob=keep_prob)
            cells_bw.append(cell_bw)

        outputs, _, _ = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(cells_fw=cells_fw,
                                                        cells_bw=cells_bw,
                                                        inputs=embeddings,
                                                        sequence_length=seq_length,
                                                        dtype=tf.float32)


    #attention layer, optional
    if use_attention:
        with tf.variable_scope("attention"):
            omega = tf.get_variable(shape=[attention_size, 1], dtype=tf.float32, name='omega',  initializer=tf.contrib.layers.xavier_initializer())
            wh = tf.tensordot(outputs, omega, axes=1) 
            wh = tf.squeeze(wh, -1) 
            u = tf.tanh(wh)  
            a = tf.exp(u)
            a = a * mask_weights
            a /= tf.reduce_sum(a, axis=1, keepdims=True) + epsilon 

            weighted_out = outputs * tf.expand_dims(a, -1)
            context_vector = tf.reduce_sum(weighted_out, axis=1)
            context_vector = tf.expand_dims(context_vector,axis=1)
            context_vector_tile = tf.tile(context_vector, [1, MAX_LENGTH, 1])

            outputs = tf.concat([outputs, context_vector_tile], 2)

    #creation of the logits and sum with the label maask
    with tf.variable_scope("dense_F"):
        logitsF = tf.layers.dense(outputs, labelF_vocab_size, activation=None, kernel_initializer=tf.contrib.layers.xavier_initializer())
        masked_logitsF = tf.add(sense_maskF, logitsF)
        masked_logitsF = tf.squeeze(masked_logitsF)

    with tf.variable_scope("dense_C"):
        logitsC = tf.layers.dense(outputs, labelC_vocab_size, activation=None, kernel_initializer=tf.contrib.layers.xavier_initializer())
        masked_logitsC = tf.add(sense_maskC, logitsC)
        masked_logitsC = tf.squeeze(masked_logitsC)
        


    #loss
    with tf.variable_scope("loss_F"):
        lossF = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labelsF, logits=masked_logitsF)
        lossF = tf.reduce_mean(tf.boolean_mask(lossF, mask_weights))

    with tf.variable_scope("loss_C"):
        lossC = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labelsC, logits=masked_logitsC)
        lossC = tf.reduce_mean(tf.boolean_mask(lossC, mask_weights))

    #combined loss
    with tf.variable_scope("loss"):
        loss = tf.add(lossC, lossF)


    #optimizer
    with tf.variable_scope("train"):
        #train_op =tf.train.MomentumOptimizer(0.001, 0.95).minimize(loss)
        train_op = tf.train.AdamOptimizer(1e-3).minimize(loss)


    #predictions
    with tf.variable_scope("prediction_F"):
        masked_labelsF = tf.boolean_mask(labelsF, mask_weights)
        predictionsF = tf.boolean_mask(tf.cast(tf.math.argmax(masked_logitsF, axis=-1), tf.int64), mask_weights)

    with tf.variable_scope("prediction_C"):
        masked_labelsC = tf.boolean_mask(labelsC, mask_weights)
        predictionsC = tf.boolean_mask(tf.cast(tf.math.argmax(masked_logitsC, axis=-1), tf.int64), mask_weights)
    
    return inputs, labelsF, labelsC, sense_maskF, sense_maskC, keep_prob, loss, train_op, predictionsF, predictionsC, masked_labelsF, masked_labelsC

def batch_generator(X, Yf, Yc, batch_size):
    """
    Reshuffle sets and split them into batches

    :param X: inputs
    :param Yf: fine-grained labelss
    :param Yc: coarse-grained labels
    :param batch_size: numb. of sentences for batches
    :return X: subset of inputs
    :return Yf: subset of fine-grained labelss
    :return Yc: subset of coarse-grained labels
    """
    # if not shuffle:
    #     for start in range(0, len(X), batch_size):
    #         end = start + batch_size
    #         yield X[start:end], Yf[start:end], Yc[start:end]
    # else:
    c = list(zip(X, Yf, Yc))
    random.shuffle(c)
    X, Yf, Yc = zip(*c)
    for start in range(0, len(X), batch_size):
        end = start + batch_size
        yield X[start:end], Yf[start:end], Yc[start:end]


def batch_generator_prediction(X, Xa, Yf, Yc, batch_size):
    """
    Split sets into batches. For evaluation phase.

    :param X: inputs
    :param Xa: inputs in another format
    :param Yf: fine-grained labelss
    :param Yc: coarse-grained labels
    :param batch_size: numb. of sentences for batches
    :return X: subset of inputs
    :return Xa: subset inputs in another format
    :return Yf: subset of fine-grained labelss
    :return Yc: subset of coarse-grained labels
    """
    for start in range(0, len(X), batch_size):
        end = start + batch_size
        yield X[start:end], Xa[start:end], Yf[start:end], Yc[start:end]



def setLabelsMask(input, labels, MAX_LENGTH, n_classes, grain, use_elmo):
    """
    Define the label mask, of shape [batch_size, max_length, n_classes]. 
    Initially its setted to  -1e8, then the position corresponing the desired label are setted to 0.

    :param input: input set
    :param labels: label set
    :param MAX_LENGTH: max length of sentences
    :param n_classes: number of possible classes
    :param grain: flag, switch between fine and coarse grained
    :param use_elmo: flag for the use of elmo
    :return mask_matrix: label mask
    """
    if use_elmo:
        if grain=='f':
            classes=loadDataset('elmo_en_classF_list.pickle')
        else:
            classes=loadDataset('elmo_en_classC_list.pickle')
        zero='<PAD>'
    else:
        if grain=='f':
            classes=loadDataset('en_classF_list.pickle')
        else:
            classes=loadDataset('en_classC_list.pickle')
        zero=0

    batch_size=len(labels)
    mask_matrix = np.ones(shape=(batch_size, MAX_LENGTH, n_classes)) * -1e8


    for id_sentence in range(batch_size):
        for id_word in range(MAX_LENGTH):
            id_sense = input[id_sentence][id_word]
            id_label = labels[id_sentence][id_word]
            if id_sense==zero:
                mask_matrix[id_sentence][id_word][0]=0.
            elif id_sense in classes:
                sense_list=classes[id_sense]
                if id_label not in sense_list and id_label != 1:
                    sense_list=np.append(sense_list, id_label)
                mask_matrix[id_sentence][id_word][sense_list]=0.
            else:
                sense_list=np.random.randint(n_classes, size=10)
                sense_list=np.append(sense_list, id_label)
                mask_matrix[id_sentence][id_word][sense_list]=0.
    
    return mask_matrix

def reduceSets(label, pred, grad):
    """
    Remove predictions and labels which not correspond to a sense

    :param label: label set
    :param pred: predictions set
    :param grain: flag, switch between fine and coarse grained
    :return red_label: reduced label set
    :return red_pred: reduced predictions set
    """
    red_label=[]
    red_pred=[]
    if grad=='f':
        vocab=loadDataset('labelF_vocab_opp.pickle')
        for e, elem in enumerate(label):
            word=vocab[elem]
            if "wn:" in str(word):
                red_label.append(elem)
                red_pred.append(pred[e])
    else:
        vocab=loadDataset('labelC_vocab_opp.pickle')
        for e, elem in enumerate(label):
            word=vocab[elem]
            if "." in str(word) and str(word) != ".":
                red_label.append(elem)
                red_pred.append(pred[e])


    return red_label, red_pred


def add_summary(writer, name, value, global_step):
    summary = tf.Summary(value=[tf.Summary.Value(tag=name, simple_value=value)])
    writer.add_summary(summary, global_step=global_step)
            
            
def evaluatePrecision(label, pred):
    """
    Evaluate precision, recall and F1 score.

    :param label: label set
    :param pred: predictions set
    :return precision: precision score
    :return recall: recall score
    :return f1: f1 score
    """

    cnf_matrix = confusion_matrix(label, pred)
    FP_list = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)  
    FN_list = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
    TP_list = np.diag(cnf_matrix)

    FP = float(FP_list.sum())
    FN = float(FN_list.sum())
    TP = float(TP_list.sum())

    # recall, or true positive rate
    recall = TP/(TP+FN)
    # Precision or positive predictive value
    precision = TP/(TP+FP)
    #F1 score
    if (precision+recall)==0:
        f1=0
    else:
        f1 = 2*precision*recall/(precision+recall)

    return recall, precision, f1


def mostFrequentSense(word, grain):
    """
    Extract the Most Frequent Sense of a given word

    :param word: given word
    :param grain: flag, switch between fine and coarse grained
    :return mfs: most frequent sense
    """
    senses = wn.synsets(word)
    mfs=1
    if grain =='f':
        vocab=loadDataset('labelF_vocab.pickle')
        for s in range(len(senses)):
            sense_wn='wn:' + str(wn.synsets(word)[s].offset()).zfill(8) + wn.synsets(word)[s].pos()
            if sense_wn in vocab:
                mfs=vocab[sense_wn]
                break
    else:
        vocab=loadDataset('labelC_vocab.pickle')
        map=loadDataset('wn2lex.pickle')
        for s in range(len(senses)):
            sense_wn='wn:' + str(wn.synsets(word)[s].offset()).zfill(8) + wn.synsets(word)[s].pos()
            lex_sense=map[sense_wn]
            if lex_sense in vocab:
                mfs=vocab[lex_sense]
                break

    
    return mfs


def checkUnknowWordElmo(data, original, prediction, g):
    """
    Check if a word is a OOV. In case replace its prediction with the MFS

    :param data: input set in encoded version
    :param original: input set in text version
    :param prediction: list of predictions
    :param g: flag, switch between fine and coarse grained
    :return prediction: list of predictions
    """
    k=0
    for l, line in enumerate(data):
        for i, item in enumerate(line):
            if item ==0: break
            elif item==1:
                word=original[l][i]
                mfs = mostFrequentSense(word, g)
                prediction[k]=mfs
            k+=1
    
    return prediction


def text_padding(text, MAX_LENGTH):
    """
    apply padding and truncation to input text.

    :param text: input set in text version
    :param MAX_LENGTH:  max length of sentences
    :return new_text: padded input set
    """
    new_text=[]
    for line in text:
        l=len(line)
        new_line=line
        while len(new_line) < MAX_LENGTH:
            new_line.append("<PAD>")
        if len(new_line) > MAX_LENGTH:
            new_line=new_line[:MAX_LENGTH]
        new_text.append(new_line)
    
    return new_text



