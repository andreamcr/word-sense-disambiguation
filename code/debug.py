import tensorflow as tf
import tensorflow.keras as K
import numpy as np 
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.sequence import pad_sequences
from argparse import ArgumentParser
import io
import random
import pickle
from utils import *

data= loadDataset('labelF_vocab_opp.pickle')



# map_wn_to_bn = dict()
# with io.open("../resources/babelnet2wordnet.tsv", "r", encoding="utf-8") as b2w:
#     for line in b2w:
#         pair=line.split()
#         map_wn_to_bn[pair[1]]=pair[0]

# map_bn_to_wnd = dict()
# with io.open("../resources/babelnet2wndomains.tsv", "r", encoding="utf-8") as b2wd:
#     for line in b2wd:
#         pair=line.split()
#         map_bn_to_wnd[pair[0]]=pair[1]

# map_wn_to_wnd=dict()
# for wn, bn in map_wn_to_bn.items():
#     if bn in map_bn_to_wnd:
#         map_wn_to_wnd[wn]=map_bn_to_wnd[bn]


# pic_map = open("wn2wnd.pickle",'wb')
# pickle.dump(map_wn_to_wnd, pic_map)
# pic_map.close()