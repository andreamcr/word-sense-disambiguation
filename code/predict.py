import tensorflow as tf
import tensorflow.keras as K
import numpy as np 
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.sequence import pad_sequences
from argparse import ArgumentParser
import io
import random
import pickle
from utils import *
from parse_semcor import *
from model_elmo import *

#DEFINE COSTANTS
MAX_LENGTH = 60
EMBEDDING_SIZE = 200
HIDDEN_SIZE = 256
epochs = 10
batch_size = 64


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_path", help="The path of the input file")
    parser.add_argument("output_path", help="The path of the output file")
    parser.add_argument("resources_path", help="The path of the resources needed to load your model")

    return parser.parse_args()


def predicting_model(input_path, resources_path, g):

    logs_path= resources_path+'logs/'

    test_set, labelF, labelC, index_list = parsing_for_predict(input_path)

    data_vocab = loadDataset("data_vocab.pickle")
    labelF_vocab = loadDataset("labelF_vocab.pickle")
    labelC_vocab = loadDataset("labelC_vocab.pickle")

    encoded_test_set = encodeDataset(test_set, data_vocab, "None")
    test_labelF = encodeDataset(labelF, labelF_vocab, "None")
    test_labelC = encodeDataset(labelC, labelC_vocab, "None")


    test_set=resizeBatch(test_set, MAX_LENGTH)
    encoded_test_set=resizeBatch(encoded_test_set, MAX_LENGTH)
    test_labelF=resizeBatch(test_labelF, MAX_LENGTH)
    test_labelC=resizeBatch(test_labelC, MAX_LENGTH)

    set_dim=len(test_set)
    

    vocab_size=len(data_vocab)
    labelF_vocab_size=len(labelF_vocab)
    labelC_vocab_size=len(labelC_vocab)

    x_test_enc = pad_sequences(encoded_test_set, truncating='post', padding='post', maxlen=MAX_LENGTH)
    y_testF = pad_sequences(test_labelF, truncating='post', padding='post', maxlen=MAX_LENGTH)
    y_testC = pad_sequences(test_labelC, truncating='post', padding='post', maxlen=MAX_LENGTH)

    n_dev_iterations = int(np.ceil(set_dim/batch_size))

    print("Importing model... ")
    inputs, labelsF, labelsC,  sense_maskF, sense_maskC, keep_prob, _, _, predictionsF, predictionsC, masked_labelsF, masked_labelsC = create_tensorflow_model(vocab_size, labelF_vocab_size, labelC_vocab_size, EMBEDDING_SIZE, HIDDEN_SIZE, True, True)
    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.initializers.local_variables())
        if tf.train.latest_checkpoint(logs_path) is not None:
            print('Restoring file: %s' %tf.train.latest_checkpoint(logs_path))
            saver.restore(sess, tf.train.latest_checkpoint(logs_path))


        total_prediction=[]
        for batch_x, batch_xE, batch_yF, batch_yC in batch_generator_prediction(test_set, x_test_enc, y_testF, y_testC, batch_size):
            batch_x = text_padding(batch_x, MAX_LENGTH)

            ls_maskF = setLabelsMask(batch_x, batch_yF,  MAX_LENGTH, labelF_vocab_size, 'f', True)
            ls_maskC = setLabelsMask(batch_x, batch_yC,  MAX_LENGTH, labelC_vocab_size, 'c', True)
    
            pred_valF, pred_valC, mask_labF, mask_labC = sess.run([predictionsF, predictionsC, masked_labelsF, masked_labelsC], feed_dict={inputs: batch_x, labelsF: batch_yF, labelsC: batch_yC, sense_maskF: ls_maskF, sense_maskC: ls_maskC, keep_prob: 1.0})
            
            pred_valF = checkUnknowWordElmo(batch_xE, batch_x, pred_valF, 'f')
            pred_valC = checkUnknowWordElmo(batch_xE, batch_x, pred_valC, 'c')

        
            if g=='f':
                for pf in pred_valF:
                    total_prediction.append(pf)
            else:
                for pc in pred_valC:
                    total_prediction.append(pc)

            

    return total_prediction, index_list


def predict_babelnet(input_path : str, output_path : str, resources_path : str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").
    
    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.
    
    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    total_prediction, index_list = predicting_model(input_path, resources_path, 'f')

    inverse_vocab=loadDataset("labelF_vocab_opp.pickle")
    w2b_map=loadDataset("wn2bn.pickle")

    with io.open(output_path,"w", encoding='utf-8') as pred_file:
        for i, id in enumerate(index_list):
            if id != 0:
                sense=inverse_vocab[total_prediction[i]]
                if sense in w2b_map:
                    bn=w2b_map[sense]
                else: bn=sense
                pred_file.write(str(id) +'\t'+str(bn)+"\n")


    print("--- Babelnet Prediction created ---")



def predict_wordnet_domains(input_path : str, output_path : str, resources_path : str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """

    total_prediction, index_list = predicting_model(input_path, resources_path, 'f')

    inverse_vocab=loadDataset("labelF_vocab_opp.pickle")
    w2d_map=loadDataset("wn2wnd.pickle")

    with io.open(output_path,"w", encoding='utf-8') as pred_file:
        for i, id in enumerate(index_list):
            if id != 0:
                sense=inverse_vocab[total_prediction[i]]
                if sense in w2d_map:
                    domain=w2d_map[sense]
                else: domain=sense #modify
                pred_file.write(str(id) +'\t'+str(domain)+"\n")

    
    print("--- WordNet domains Prediction created ---")

def predict_lexicographer(input_path : str, output_path : str, resources_path : str) -> None:

    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    
    total_prediction, index_list = predicting_model(input_path, resources_path, 'c')

    inverse_vocab=loadDataset("labelC_vocab_opp.pickle")

    with io.open(output_path,"w", encoding='utf-8') as pred_file:
        for i, id in enumerate(index_list):
            if id != 0:
                sense=inverse_vocab[total_prediction[i]]
                pred_file.write(str(id) +'\t'+str(sense)+"\n")

    
    print("--- Lexicographer Prediction created ---")


if __name__ == '__main__':
    args = parse_args()


    predict_babelnet(args.input_path, args.output_path+'predict_babelnet.txt', args.resources_path)
    #predict_wordnet_domains(args.input_path, args.output_path+'predict_wordnet_domains.txt', args.resources_path)
    #predict_lexicographer(args.input_path, args.output_path+'predict_lexicographer.txt', args.resources_path)

