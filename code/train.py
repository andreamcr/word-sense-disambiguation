import tensorflow as tf
import tensorflow.keras as K
import numpy as np 
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.sequence import pad_sequences
from argparse import ArgumentParser
import io
import random
import pickle
from utils import *
from model_elmo import *


#flag for the use (or not) of elmo embeddings and/or attention layer
use_elmo = True
use_attention = True

#DEFINE COSTANTS
MAX_LENGTH = 60
EMBEDDING_SIZE = 200
HIDDEN_SIZE = 256
epochs = 10
batch_size = 64

logs_path= '../resources/logs'
model_name= 'final_model'


if __name__ == '__main__':


    #load the dataset, in text or ID version.
    if use_elmo:
        x_train = loadDataset("train_data.pickle")
        x_dev = loadDataset("dev_data.pickle")

        additional_set = loadDataset("dev_set_encoded.pickle")
        additional_set = pad_sequences(additional_set, truncating='post', padding='post', maxlen=MAX_LENGTH)

    else:
        train_set = loadDataset("train_set_encoded.pickle")
        dev_set = loadDataset("dev_set_encoded.pickle")
        
        x_train = pad_sequences(train_set, truncating='post', padding='post', maxlen=MAX_LENGTH)
        x_dev = pad_sequences(dev_set, truncating='post', padding='post', maxlen=MAX_LENGTH)

        additional_set = loadDataset("dev_data.pickle")
    
    #two lists for the labels, one will be used for fine-grained while the other for coarse-grained
    train_labelF = loadDataset("train_labelF_encoded.pickle")
    train_labelC = loadDataset("train_labelC_encoded.pickle")

    dev_labelF = loadDataset("dev_labelF_encoded.pickle")
    dev_labelC = loadDataset("dev_labelC_encoded.pickle")

    train_vocab = loadDataset("data_vocab.pickle")
    labelF_vocab = loadDataset("labelF_vocab.pickle")
    labelC_vocab = loadDataset("labelC_vocab.pickle")

    #size of all sets and vocabulary
    x_train_dim=len(x_train)
    x_dev_dim=len(x_dev)
    train_vocab_size=len(train_vocab)
    labelF_vocab_size=len(labelF_vocab)
    labelC_vocab_size=len(labelC_vocab)

    print("vocabs: ", train_vocab_size, "\t", labelF_vocab_size, "\t", labelC_vocab_size)

    #padding of label lists
    y_trainF = pad_sequences(train_labelF, truncating='post', padding='post', maxlen=MAX_LENGTH)
    y_trainC = pad_sequences(train_labelC, truncating='post', padding='post', maxlen=MAX_LENGTH)

    y_devF = pad_sequences(dev_labelF, truncating='post', padding='post', maxlen=MAX_LENGTH)
    y_devC = pad_sequences(dev_labelC, truncating='post', padding='post', maxlen=MAX_LENGTH)

    n_iterations = int(np.ceil(x_train_dim/batch_size))
    n_dev_iterations = int(np.ceil(x_dev_dim/batch_size))


    print("Creating model... ")
    inputs, labelsF, labelsC, sense_maskF, sense_maskC, keep_prob, loss, train_op, predictionsF, predictionsC, masked_labelsF, masked_labelsC = create_tensorflow_model(train_vocab_size, labelF_vocab_size, labelC_vocab_size, EMBEDDING_SIZE, HIDDEN_SIZE, use_attention, use_elmo)

    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.initializers.local_variables())
        #Load latest checkpoint if available
        if tf.train.latest_checkpoint(logs_path) is not None:
            print('Restoring file: %s' %tf.train.latest_checkpoint(logs_path))
            saver.restore(sess, tf.train.latest_checkpoint(logs_path))
        #Create summaries for tensorboard
        train_writer = tf.summary.FileWriter('../resources/tensorflow_model', sess.graph)
        print("\nStarting training...")
        for epoch in range(epochs):
            print("\nEpoch", epoch + 1)
            epoch_loss, epoch_preF, epoch_recF, epoch_f1F, epoch_preC, epoch_recC, epoch_f1C = 0., 0., 0., 0., 0., 0., 0.
            mb = 0
            print("======="*10)
            for batch_x, batch_yF, batch_yC in batch_generator(x_train, y_trainF, y_trainC, batch_size):
                if use_elmo:
                    #padding for text version
                    batch_x = text_padding(batch_x, MAX_LENGTH)

                #define label masks for both classification
                ls_maskF = setLabelsMask(batch_x, batch_yF,  MAX_LENGTH, labelF_vocab_size, 'f', use_elmo)
                ls_maskC = setLabelsMask(batch_x, batch_yC,  MAX_LENGTH, labelC_vocab_size, 'c', use_elmo)
                
                mb += 1
                _, loss_val, pred_valF, pred_valC, mask_labF, mask_labC = sess.run([train_op, loss, predictionsF, predictionsC, masked_labelsF, masked_labelsC], 
                                                 feed_dict={inputs: batch_x, labelsF: batch_yF, labelsC: batch_yC, sense_maskF: ls_maskF, sense_maskC: ls_maskC, keep_prob: 0.8})
                
                #reduce the predictions and the labels only to word tokens with senses
                red_labelF, red_predF = reduceSets(mask_labF, pred_valF, 'f')
                red_labelC, red_predC = reduceSets(mask_labC, pred_valC, 'c')

                #evaluate the scores
                recallF, precisionF, f1F = evaluatePrecision(red_labelF, red_predF)
                recallC, precisionC, f1C = evaluatePrecision(red_labelC, red_predC)

                epoch_loss += loss_val
                epoch_preF += precisionF
                epoch_recF += recallF
                epoch_f1F += f1F
                epoch_preC += precisionC
                epoch_recC += recallC
                epoch_f1C += f1C
                
                print("{:.2f}%\tTrain Loss: {:.4f}\tFine P: {:.4f}\t R: {:.4f}\t F1: {:.4f}\tCorse P: {:.4f}\t R: {:.4f}\t F1: {:.4f}  ".format(100.*mb/n_iterations, epoch_loss/mb, epoch_preF/mb, epoch_recF/mb, epoch_f1F/mb, epoch_preC/mb, epoch_recC/mb, epoch_f1C/mb), end="\r")

            epoch_loss /= n_iterations

            epoch_preF /= n_iterations
            epoch_recF /= n_iterations
            epoch_f1F /= n_iterations
            epoch_preC /= n_iterations
            epoch_recC /= n_iterations
            epoch_f1C /= n_iterations


            add_summary(train_writer, "epoch_loss", epoch_loss, epoch)
            add_summary(train_writer, "epoch_preF", epoch_preF, epoch)
            add_summary(train_writer, "epoch_recF", epoch_recF, epoch)
            add_summary(train_writer, "epoch_f1F", epoch_f1F, epoch)
            add_summary(train_writer, "epoch_preC", epoch_preC, epoch)
            add_summary(train_writer, "epoch_recC", epoch_recC, epoch)
            add_summary(train_writer, "epoch_f1C", epoch_f1C, epoch)

            dev_loss, dev_preF, dev_recF, dev_f1F, dev_preC, dev_recC, dev_f1C = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            #for batch_x, batch_yF, batch_yC, batch_c in batch_generator(x_dev, y_devF, y_devC, batch_size):
            for batch_x, batch_xA, batch_yF, batch_yC in batch_generator_prediction(x_dev, additional_set, y_devF, y_devC, batch_size):
                if use_elmo:
                    #padding for text version
                    batch_x = text_padding(batch_x, MAX_LENGTH)
                    encoded_set = batch_xA
                    text_set=batch_x
                else:
                    text_set=batch_xA
                    text_set = text_padding(text_set, MAX_LENGTH)
                    encoded_set=batch_x

                #define label masks for both classification
                ls_maskF = setLabelsMask(batch_x, batch_yF,  MAX_LENGTH, labelF_vocab_size, 'f', use_elmo)
                ls_maskC = setLabelsMask(batch_x, batch_yC,  MAX_LENGTH, labelC_vocab_size, 'c', use_elmo)
                
                loss_val, pred_valF, pred_valC, mask_labF, mask_labC = sess.run([loss, predictionsF, predictionsC, masked_labelsF, masked_labelsC], feed_dict={inputs: batch_x, labelsF: batch_yF, labelsC: batch_yC, sense_maskF: ls_maskF, sense_maskC: ls_maskC, keep_prob: 1.0})
            
                #check if some words are OOV then change their prediction with MFS
                pred_valF = checkUnknowWordElmo(encoded_set, text_set, pred_valF, 'f')
                pred_valC = checkUnknowWordElmo(encoded_set, text_set, pred_valC, 'c')

                #reduce the predictions and the labels only to word tokens with senses
                red_labelF, red_predF = reduceSets(mask_labF, pred_valF, 'f')
                red_labelC, red_predC = reduceSets(mask_labC, pred_valC, 'c')

                #evaluate scores
                recallF, precisionF, f1F = evaluatePrecision(red_labelF, red_predF)
                recallC, precisionC, f1C = evaluatePrecision(red_labelC, red_predC)

                dev_loss += loss_val
                dev_preF += precisionF
                dev_recF += recallF
                dev_f1F += f1F
                dev_preC += precisionC
                dev_recC += recallC
                dev_f1C += f1C

            dev_loss /= n_dev_iterations
            dev_preF /= n_dev_iterations
            dev_recF /= n_dev_iterations
            dev_f1F /= n_dev_iterations
            dev_preC /= n_dev_iterations
            dev_recC /= n_dev_iterations
            dev_f1C /= n_dev_iterations

            print("\nDev Loss: {:.4f}\tFine P: {:.4f}\t R: {:.4f}\t F1: {:.4f}\tCorse P: {:.4f}\t R: {:.4f}\t F1: {:.4f}".format(dev_loss, dev_preF, dev_recF, dev_f1F, dev_preC, dev_recC, dev_f1C))
            
            add_summary(train_writer, "epoch_loss", dev_loss, epoch)

            add_summary(train_writer, "epoch_preF", dev_preF, epoch)
            add_summary(train_writer, "epoch_recF", dev_recF, epoch)
            add_summary(train_writer, "epoch_f1F", dev_f1F, epoch)
            add_summary(train_writer, "epoch_preC", dev_preC, epoch)
            add_summary(train_writer, "epoch_recC", dev_recC, epoch)
            add_summary(train_writer, "epoch_f1C", dev_f1C, epoch)

            saver.save(sess, os.path.join(logs_path, model_name))
            print("model saved")
            print("======="*10)
        train_writer.close()
